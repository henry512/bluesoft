Prueba Tecnica - Bluesoft - Biblioteca

Backend =
	.net core 2.2
	entity framework core 2.2
	
	instrucciones =
		1. Abra la solucion en Microsoft Visual Studio Community 2019 y compile
		2. Cree una base de datos y proporcione/modifique la conexion en el archivo [ApplicationDbContext] en el proyecto [infrastructure]
		3. En la consola del administrador de paquetes, ejecute > [Update-Database]
		4. Ejecute el proyecto de [Api]




Frontend =
	angular 9.0.1
	Bootstrap 4
	Jquery

	instrucciones =
		1. Acceda atraves de consola al proeycto [App]
		2. Ejecute el comando > [npm install]
		3. Ejecute el comando > [ng serve -o] para ejecutar el proyecto