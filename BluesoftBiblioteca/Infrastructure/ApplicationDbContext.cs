﻿using Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure
{
    public class ApplicationDbContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=DESKTOP-9085RON; Initial Catalog=bluesoft_biblioteca; Integrated Security=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<AutorLibro>().HasKey(x => new { x.AutorId, x.LibroId, });
            //modelBuilder.Entity<CategoriaLibro>().HasKey(x => new { x.CategoriaId, x.LibroId });

            modelBuilder.Entity<AutorLibro>()
                .HasOne<Autor>(x => x.Autor)
                .WithMany(x => x.AutorLibros)
                .HasForeignKey(x => x.AutorId);

            modelBuilder.Entity<AutorLibro>()
                .HasOne<Libro>(x => x.Libro)
                .WithMany(x => x.AutorLibros)
                .HasForeignKey(x => x.LibroId);

            modelBuilder.Entity<CategoriaLibro>()
                .HasOne<Categoria>(x => x.Categoria)
                .WithMany(x => x.CategoriaLibros)
                .HasForeignKey(x => x.CategoriaId);

            modelBuilder.Entity<CategoriaLibro>()
                .HasOne<Libro>(x => x.Libro)
                .WithMany(x => x.CategoriaLibros)
                .HasForeignKey(x => x.LibroId);

        }

        public DbSet<Autor> Autores { get; set; }
        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<Libro> Libros { get; set; }
        public DbSet<AutorLibro> AutorLibros { get; set; }
        public DbSet<CategoriaLibro> CategoriaLibros { get; set; }

    }
}
