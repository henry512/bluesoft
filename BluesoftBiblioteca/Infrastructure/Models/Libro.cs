﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Infrastructure.Models
{
    public class Libro
    {
        public int Id { get; set; }

        [Required]
        [StringLength(200)]
        public string Nombre { get; set; }

        public string Isbn { get; set; }

        public List<AutorLibro> AutorLibros { get; set; }
        public List<CategoriaLibro> CategoriaLibros { get; set; }
    }
}
