﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Models
{
    public class CategoriaLibro
    {
        public int Id { get; set; }
        public int CategoriaId { get; set; }
        public Categoria Categoria { get; set; }

        public int LibroId { get; set; }
        public Libro Libro { get; set; }
    }
}
