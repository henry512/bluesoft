import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LibrosComponent } from './components/libros.component';
import { AutoresComponent } from './components/autores.component';
import { CategoriasComponent } from './components/categorias.component';

const routes: Routes = [
  { path: "", component: LibrosComponent },
  { path: "autores", component: AutoresComponent },
  { path: "categorias", component: CategoriasComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
