import { Component } from "@angular/core";
import { CategoriasService } from "../services/categorias.services";
import { Categorias } from '../models/categorias';
import { IEditable } from '../models/IEditable';
import { FormGroupName } from '@angular/forms';

@Component({
  selector: "app-categorias",
  templateUrl: "../views/categorias.html",
  providers: [CategoriasService]
})
export class CategoriasComponent {

  public categorias: Categorias[];
  public categoriasEdit: IEditable[];
  public categoria: Categorias;
  public categoriaTemporal: Categorias;
  public cargando: boolean;

  constructor(private _categoriasService: CategoriasService) {
    this.cargando = false;
    this.categorias = [];
    this.categoriasEdit = [];
    this.categoria = new Categorias(undefined, "", "");
    this.categoriaTemporal = new Categorias(undefined, "", "");
  }

  ngOnInit() {
    this._categoriasService.getAllCategorias().subscribe(
      (data) => {
        this.categorias = data;

        this.categorias.forEach(item => {
          this.categoriasEdit.push({
            id: item.id,
            edicion: false
          })
        });

        console.log(this.categorias);
        console.log(this.categoriasEdit);
      },
      (error) => console.error(error)
    );

  }

  public activeModeEdit(id: number): void{
    this.categoriasEdit.find(item => item.id === id).edicion = true;

    let _categoria = this.categorias.find(item => item.id ===id);
    this.categoriaTemporal = new Categorias(_categoria.id, _categoria.nombre, _categoria.descripcion);
    
    console.log(_categoria);
  }

  public cancelEdit(id: number) {
    this.desactiveModeEdit(id);

    let _categoria = this.categorias.find(item => item.id ===id);
    _categoria.nombre = this.categoriaTemporal.nombre;
    _categoria.descripcion = this.categoriaTemporal.descripcion;

    this.categoriaTemporal = new Categorias(undefined, "", "");
  }

  public desactiveModeEdit(id: number): void{   
    this.categoriasEdit.find(item => item.id == id).edicion = false;
  }

  public stateEdit(id: number): boolean {
    return this.categoriasEdit.find(item => item.id == id).edicion;
  }

  public update(id: number) {
    this._categoriasService.updateCategoria(this.categorias.find(x => x.id === id)).subscribe(
      () => {
        this.desactiveModeEdit(id);
      },
      (error) => console.error(error)
    );
  }

  public delete(id: number): void {
    let confirmDelete = confirm("Seguro que quieres borrar la categoria?")

    if (confirmDelete) {
      this._categoriasService.deleteCategoria(id).subscribe(
        () => {
          this.categorias.splice(this.categorias.indexOf(this.categorias.find(item => item.id === id)), 1);
          this.categoriasEdit.splice(this.categoriasEdit.indexOf(this.categoriasEdit.find(item => item.id === id)), 1);
          this.cargando = false;
        },
        (error) => console.error(error)
      ); 
    }

  }

  create() {
    this._categoriasService.createCategoria(this.categoria).subscribe(
      (data) => {
        if (data !== undefined) {
          this.categorias.push(data);
          this.categoriasEdit.push({
            id: data.id,
            edicion: false
          })
          this.categoria = new Categorias(undefined, "", "");
        }
      },
      (error) => console.error(error)
    );
    
    console.log(this.categoria);
  }

}
