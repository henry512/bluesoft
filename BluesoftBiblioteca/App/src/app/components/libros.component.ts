import { Component } from "@angular/core";
import { LibroServices } from '../services/libros.services';
import { Libros } from '../models/libros';
import { Autores } from '../models/autores';
import { Categorias } from '../models/categorias';
import { AutoresService } from '../services/autores.services';
import { CategoriasService } from '../services/categorias.services';
import { LibroDetail } from '../models/libroDetail';
import { AutorLibro } from '../models/autorLibro';
import { CategoriaLibro } from '../models/categoriaLibro';

@Component({
  selector: "app-libros",
  templateUrl: "../views/libros.html",
  providers: [LibroServices, AutoresService, CategoriasService]
})
export class LibrosComponent {

  public libro: Libros;
  public libros: Libros[];
  public autores: Autores[];
  public categorias: Categorias[];
  public librosDetail: LibroDetail[];
  public createAutores: Autores[];
  public createCategorias: Categorias[];
  public buscadorLibro: string;
  public buscadorIsActive: boolean;
  public librosDetailBuscador: LibroDetail[];
  public libroDetailEdit: LibroDetail;
  public editAutores: Autores[];
  public editCategorias: Categorias[];

  public deleteTemporalAutorId: number[];
  public createTemporalAutorId: number[];
  public deleteTemporalCategoriaId: number[];
  public createTemporalCategoriaId: number[];

  constructor(
    private _librosServices: LibroServices,
    private _autoresServices: AutoresService,
    private _categoriasServices: CategoriasService
    ) {
      this.libro = new Libros(undefined, "", "");
      this.libros = [];
      this.autores = [];
      this.categorias = [];
      this.librosDetail = [];
      this.buscadorLibro = "";
      this.createAutores = [];
      this.createCategorias = [];
      this.buscadorIsActive = false;
      this.librosDetailBuscador = [];
      this.libroDetailEdit = new LibroDetail(undefined, "", "", [], []);
      this.editAutores = [];
      this.editCategorias = [];

      this.deleteTemporalAutorId = [];
      this.createTemporalAutorId = [];
      this.deleteTemporalCategoriaId = [];
      this.createTemporalCategoriaId = [];
  }

  ngOnInit() {
    this._librosServices.getAllLibros().subscribe(
      (data) => {
        this.libros = data;

        data.forEach(item => {
          let _libroDetail: LibroDetail = {
            id: item.id,
            nombre: item.nombre,
            isbn: item.isbn
          }

          this._librosServices.getAutoresLibro(item.id).subscribe(
            (data) => _libroDetail.autores = data
          );

          this._librosServices.getCategoriasLibro(item.id).subscribe(
            (data) => _libroDetail.categorias = data
          );

          this.librosDetail.push(_libroDetail)

        });

        console.log(this.librosDetail);
      },
      (error) => console.error(error)
    );

    this._autoresServices.getAllAutores().subscribe(
      (data) => {
        this.autores = data;
        console.log(this.autores);
      },
      (error) => console.error(error)
    );

    this._categoriasServices.getAllCategorias().subscribe(
      (data) => {
        this.categorias = data;
        console.log(this.categorias);
      },
      (error) => console.error(error)
    );
  }

  public createLibro() {
    let _libro = this.libro;
    let _autoresLibro = this.createAutores;
    let _categoriasLibro = this.createCategorias;

    console.log(_libro);
    console.log(_autoresLibro);
    console.log(_categoriasLibro);

    this._librosServices.createLibros(_libro).subscribe(
      (data) => {

        _autoresLibro.forEach(x => {
          this._librosServices.createAutoresLibro(data.id, new AutorLibro(x.id, data.id)).subscribe(null, error => console.error(error));
        });

        _categoriasLibro.forEach(x => {
          this._librosServices.createCategoriasLibro(data.id, new CategoriaLibro(x.id, data.id)).subscribe(null, error => console.error(error));
        });
        
        this.librosDetail.push(new LibroDetail(
          data.id,
          data.nombre,
          data.isbn,
          _autoresLibro,
          _categoriasLibro
        ));

      },
      (error) => console.error(error)
    );

    this.createAutores = [];
    this.createCategorias = [];
  }

  public buscar() {

    if (this.buscadorLibro.length > 0) {
      this.buscadorIsActive = true;
    } else {
      this.buscadorIsActive = false
    }

    let result: LibroDetail[] = [];

    // busqueda por nombre
    let resultNombre = this.librosDetail.filter(value => value.nombre.includes(this.buscadorLibro));

    //busqueda por autor
    let resultAutor = this.librosDetail
      .filter(value => value.autores.find(value2 => value2.nombres.includes(this.buscadorLibro) || value2.apellidos.includes(this.buscadorLibro)));

    // busqueda por categoria
    let resultCategoria = this.librosDetail
    .filter(value => value.categorias.find(value2 => value2.nombre.includes(this.buscadorLibro)));

    // hacemos push al array [result]
    resultNombre.forEach(x => result.push(x))
    resultAutor.forEach(x => result.push(x))
    resultCategoria.forEach(x => result.push(x))

    // filtramos y eliminamos los repetidos
    var hash = {};
    result = result.filter(function(current) {
      var exists = !hash[current.id] || false;
      hash[current.id] = true;
      return exists;
    });

    this.librosDetailBuscador = result;

    console.log(result);
  }

  public delete(id: number) {
    let confirmDelete = confirm("Seguro que quieres borrar el libro?")

    if (confirmDelete) {

      let libroId: number = id;
      let autoresId: number[] = [];
      let categoriasId: number[] = [];

      this.librosDetail.filter(libro => libro.id === libroId).forEach(item => {
        item.autores.forEach(autor => autoresId.push(autor.id));
        item.categorias.forEach(categoria => categoriasId.push(categoria.id));
      });

      // borrar relaciones categorias y autores
      autoresId.forEach(id => {
        this._librosServices.deleteAutoresLibro(libroId, id).subscribe(null, (error) => console.error(error));
      })
      categoriasId.forEach(id => {
        this._librosServices.deleteCategoriasLibro(libroId, id).subscribe(null, (error) => console.error(error));
      })

      //borrar registro libro
      this._librosServices.deleteLibros(libroId).subscribe(
        () => {
          this.librosDetail.splice(this.librosDetail.indexOf(this.librosDetail.find(item => item.id === libroId)), 1);
        },
        (error) => console.error(error)
      );

      console.log("libroId: " + libroId)
      console.log("autoresId: " + autoresId)
      console.log("categoriasId: " + categoriasId)
    }
  }

  public editLibro(libroId: number) {
    let _libroDetail = this.librosDetail.find(x => x.id === libroId);

    this.editAutores = [];
    this.editCategorias = [];
    this.libroDetailEdit = new LibroDetail(undefined, "", "", [], []);
    this.refreshTemporalAutorCategoria();

    _libroDetail.autores.forEach(x => {
      this.editAutores.push(x);
    });

    _libroDetail.categorias.forEach(x => {
      this.editCategorias.push(x);
    });

    this.libroDetailEdit = new LibroDetail(
      _libroDetail.id, 
      _libroDetail.nombre,
      _libroDetail.isbn,
      this.editAutores,
      this.editCategorias)
  }

  public updateLibro() {
    let _libroDetailEdit = this.libroDetailEdit;
    let _libroDetail = this.librosDetail.find(x => x.id === _libroDetailEdit.id);
    let _libro = new Libros(_libroDetailEdit.id, _libroDetailEdit.nombre, _libroDetailEdit.isbn);

    this._librosServices.updateLibros(_libro).subscribe(
      () => {
        _libroDetail.nombre = _libro.nombre;
        _libroDetail.isbn = _libro.isbn;
        _libroDetail.autores = _libroDetailEdit.autores;
        _libroDetail.categorias = _libroDetailEdit.categorias;
      }, 
      (error) => console.error(error)
    );

    // actualizar relaciones autores/categorias

    
    
    console.log(this.libroDetailEdit.id);
    console.log(this.libroDetailEdit.nombre);
    console.log(this.libroDetailEdit.isbn);
    console.log(this.libroDetailEdit.autores);
    console.log(this.libroDetailEdit.categorias);

    console.log(this.deleteTemporalAutorId);

    this.refreshTemporalAutorCategoria();
  }

  public deleteTemporalAutor(id: number) {
    this.deleteTemporalAutorId.push(id);
  }
  public createTemporalAutor(id: number) {
    this.createTemporalAutorId.push(id);
  }
  public deleteTemporalCategoria(id: number) {
    this.deleteTemporalCategoriaId.push(id);
  }
  public createTemporalCategoria(id: number) {
    this.createTemporalCategoriaId.push(id);
  }

  public refreshTemporalAutorCategoria() {
    this.deleteTemporalAutorId = [];
    this.createTemporalAutorId = [];
    this.deleteTemporalCategoriaId = [];
    this.createTemporalCategoriaId = [];
  }

}
