import { Component } from "@angular/core";
import { Autores } from '../models/autores';
import { IEditable } from '../models/IEditable';
import { AutoresService } from '../services/autores.services';

@Component({
  selector: "app-autores",
  templateUrl: "../views/autores.html",
  providers: [AutoresService]
})
export class AutoresComponent {
  public autores: Autores[];
  public autoresEdit: IEditable[];
  public autor: Autores;
  public autorTemporal: Autores;
  public cargando: boolean;

  constructor(private _autoresService: AutoresService) {
    this.cargando = false;
    this.autores = [];
    this.autoresEdit = [];
    this.autor = new Autores(undefined, "", "", undefined);
    this.autorTemporal = new Autores(undefined, "", "", undefined);
  }

  ngOnInit() {
    this._autoresService.getAllAutores().subscribe(
      (data) => {
        this.autores = data;

        this.autores.forEach(item => {
          this.autoresEdit.push({
            id: item.id,
            edicion: false
          })
        });

        console.log(this.autores);
        console.log(this.autoresEdit);
      },
      (error) => console.error(error)
    );

  }

  public activeModeEdit(id: number): void{
    this.autoresEdit.find(item => item.id === id).edicion = true;

    let _autor = this.autores.find(item => item.id ===id);
    this.autorTemporal = new Autores(_autor.id, _autor.nombres, _autor.apellidos, _autor.fecha);
    
    console.log(_autor);
  }

  public cancelEdit(id: number) {
    this.desactiveModeEdit(id);

    let _autor = this.autores.find(item => item.id ===id);
    _autor.nombres = this.autorTemporal.nombres;
    _autor.apellidos = this.autorTemporal.apellidos;
    _autor.fecha = this.autorTemporal.fecha;

    this.autorTemporal = new Autores(undefined, "", "", undefined);
  }

  public desactiveModeEdit(id: number): void{   
    this.autoresEdit.find(item => item.id == id).edicion = false;
  }

  public stateEdit(id: number): boolean {
    return this.autoresEdit.find(item => item.id == id).edicion;
  }

  public update(id: number) {
    this._autoresService.updateAutores(this.autores.find(x => x.id === id)).subscribe(
      () => {
        this.desactiveModeEdit(id);
      },
      (error) => console.error(error)
    );
  }

  public delete(id: number): void {
    let confirmDelete = confirm("Seguro que quieres borrar la categoria?")

    if (confirmDelete) {
      this._autoresService.deleteAutores(id).subscribe(
        () => {
          this.autores.splice(this.autores.indexOf(this.autores.find(item => item.id === id)), 1);
          this.autoresEdit.splice(this.autoresEdit.indexOf(this.autoresEdit.find(item => item.id === id)), 1);
          this.cargando = false;
        },
        (error) => console.error(error)
      ); 
    }

  }

  create() {
    this._autoresService.createAutores(this.autor).subscribe(
      (data) => {
        if (data !== undefined) {
          this.autores.push(data);
          this.autoresEdit.push({
            id: data.id,
            edicion: false
          })
          this.autor = new Autores(undefined, "", "", undefined);
        }
      },
      (error) => console.error(error)
    );
    
    console.log(this.autor);
  }
}
