import { Autores } from './autores';
import { Categorias } from './categorias';

export class LibroDetail {
    constructor(
        public id: number,
        public nombre: string,
        public isbn: string,
        public autores?: Autores[],
        public categorias?: Categorias[]
    ) {}
}