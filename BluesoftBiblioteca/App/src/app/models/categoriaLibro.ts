export class CategoriaLibro {
    constructor(
        public categoriaId: number,
        public libroId: number,
    ) {}
}