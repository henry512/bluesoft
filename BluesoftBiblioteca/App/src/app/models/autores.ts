export class Autores {
    constructor(
        public id: number,
        public nombres: string,
        public apellidos: string,
        public fecha: Date,
    ) {}
}