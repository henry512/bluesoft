export class Libros {
    constructor(
        public id: number,
        public nombre: string,
        public isbn: string
    ) {}
}