export interface IEditable {
    id: number,
    edicion: boolean
}