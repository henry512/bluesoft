import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
import { Libros } from '../models/libros';
import { Autores } from '../models/autores';
import { Categorias } from '../models/categorias';
import { CategoriaLibro } from '../models/categoriaLibro';
import { AutorLibro } from '../models/autorLibro';

@Injectable({
    providedIn: 'root'
})
export class LibroServices {
    private _URL: string = "https://localhost:44341/api/libro";

    constructor(private http: HttpClient) { }

    public getAllLibros(): Observable<Libros[]> {
        return this.http.get<Libros[]>(this._URL);
    }

    public getCategoriasLibro(id: number): Observable<Categorias[]> {
        return this.http.get<Categorias[]>(this._URL + "/" + id + "/categorias");
    }

    public getAutoresLibro(id: number): Observable<Autores[]> {
        return this.http.get<Autores[]>(this._URL + "/" + id + "/autores");
    }

    public createCategoriasLibro(id: number, categoriaLibro: CategoriaLibro): Observable<any> {
        return this.http.post<any>(this._URL + "/" + id + "/categorias", categoriaLibro);
    }

    public createAutoresLibro(id: number, autorLibro: AutorLibro): Observable<any> {
        return this.http.post<any>(this._URL + "/" + id + "/autores", autorLibro);
    }

    public deleteCategoriasLibro(id: number, categoriaId: number): Observable<any> {
        return this.http.delete<any>(this._URL + "/" + id + "/categorias/" + categoriaId);
    }

    public deleteAutoresLibro(id: number, autorId: number): Observable<any> {
        return this.http.delete<any>(this._URL + "/" + id + "/autores/" + autorId);
    }

    public createLibros(libro: Libros): Observable<Libros> {
        return this.http.post<Libros>(this._URL, libro);
    }

    public deleteLibros(id: number): Observable<any> {
        return this.http.delete<any>(this._URL + "/"+id);
    }

    public updateLibros(libro: Libros): Observable<any> {
        return this.http.put(this._URL + "/" + libro.id, libro);
    }
}