import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { Categorias } from '../models/categorias';

@Injectable({
  providedIn: 'root',
})
export class CategoriasService {

  private _URL: string = "https://localhost:44341/api/categoria";

  constructor(private http: HttpClient) { }

  public getAllCategorias(): Observable<Categorias[]> {
    return this.http.get<Categorias[]>(this._URL);
  }

  public createCategoria(categoria: Categorias): Observable<Categorias> {
    return this.http.post<Categorias>(this._URL, categoria);
  }

  public deleteCategoria(id: number): Observable<any> {
    return this.http.delete<any>(this._URL + "/"+id);
  }

  public updateCategoria(categoria: Categorias): Observable<any> {
    return this.http.put(this._URL + "/" + categoria.id, categoria);
  }

}
