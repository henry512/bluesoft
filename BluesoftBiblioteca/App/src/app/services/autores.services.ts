import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { Autores } from '../models/autores';

@Injectable({
  providedIn: 'root',
})
export class AutoresService {
    private _URL: string = "https://localhost:44341/api/autor";

    constructor(private http: HttpClient) { }

    public getAllAutores(): Observable<Autores[]> {
        return this.http.get<Autores[]>(this._URL);
    }

    public createAutores(autor: Autores): Observable<Autores> {
        return this.http.post<Autores>(this._URL, autor);
    }

    public deleteAutores(id: number): Observable<any> {
        return this.http.delete<any>(this._URL + "/"+id);
    }

    public updateAutores(autor: Autores): Observable<any> {
        return this.http.put(this._URL + "/" + autor.id, autor);
    }
}