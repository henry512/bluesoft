﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Dto
{
    public class CategoriasDto
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
    }
}
