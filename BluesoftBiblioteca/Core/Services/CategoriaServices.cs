﻿using Core.Contracts;
using Infrastructure;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Services
{
    public class CategoriaServices : IServices<Categoria>
    {
        public async Task<Categoria> Create(Categoria categoria)
        {
            Categoria result = null;

            using (var context = new ApplicationDbContext())
            {
                context.Categorias.Add(categoria);

                try
                {
                    await context.SaveChangesAsync();
                    result = categoria;
                }
                catch (Exception)
                {
                    throw;
                }
            }

            return result;
        }

        public async Task<bool> Delete(int id)
        {
            var result = false;
            Categoria categoria = Find(id);

            if (categoria == null)
            {
                return result;
            }
            else
            {
                using (var context = new ApplicationDbContext())
                {
                    context.Categorias.Remove(categoria);

                    try
                    {
                        await context.SaveChangesAsync();
                        result = true;
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }

            return result;
        }

        public Categoria Find(int id)
        {
            Categoria result = null;

            using (var conext = new ApplicationDbContext())
            {
                try
                {
                    result = conext.Categorias.FirstOrDefault(x => x.Id == id);
                }
                catch (Exception)
                {
                    throw;
                }
            }

            return result;
        }

        public List<Categoria> FindAll()
        {
            List<Categoria> result = new List<Categoria>();

            using (var conext = new ApplicationDbContext())
            {
                try
                {
                    result = conext.Categorias.ToList();
                }
                catch (Exception)
                {
                    throw;
                }
            }

            return result;
        }

        public async Task<bool> Update(Categoria categoria)
        {
            bool result = false;
            Categoria _categoria = Find(categoria.Id);

            if (_categoria == null)
            {
                return result;
            } 
            else 
            {
                using (var conext = new ApplicationDbContext())
                {
                    conext.Categorias.Update(categoria);

                    try
                    {
                        await conext.SaveChangesAsync();
                        result = true;
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }

            return result;
        }
    }
}
