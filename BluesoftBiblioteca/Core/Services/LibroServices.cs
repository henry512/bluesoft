﻿using Core.Contracts;
using Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Infrastructure;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Core.Dto;

namespace Core.Services
{
    public class LibroServices : IServices<Libro>
    {
        public async Task<Libro> Create(Libro libro)
        {
            Libro result = null;

            using (var context = new ApplicationDbContext())
            {
                context.Libros.Add(libro);

                try
                {
                    await context.SaveChangesAsync();
                    result = libro;
                }
                catch (Exception)
                {
                    throw;
                }
            }

            return result;
        }

        public List<AutoresDto> FindAutores(int libroId)
        {
            List<AutoresDto> autores = null;

            using (var context = new ApplicationDbContext())
            {
                try
                {
                    List<AutorLibro> autoresLibro = context.AutorLibros
                        .Where(x => x.LibroId == libroId)
                        .Include(x => x.Autor)
                        .ToList();

                   autores = autoresLibro
                        .Select(x => x.Autor)
                        .Select(x => new AutoresDto() { 
                            Id = x.Id, 
                            Nombres = x.Nombres, 
                            Apellidos = x.Apellidos, 
                            Fecha = x.Fecha
                        })
                        .ToList();
                }
                catch (Exception)
                {
                    throw;
                }
            }

            return autores;
        }

        public List<CategoriasDto> FindCategorias(int libroId)
        {
            List<CategoriasDto> autores = null;

            using (var context = new ApplicationDbContext())
            {
                try
                {
                    List<CategoriaLibro> categoriasLibro = context.CategoriaLibros
                        .Where(x => x.LibroId == libroId)
                        .Include(x => x.Categoria)
                        .ToList();

                    autores = categoriasLibro
                         .Select(x => x.Categoria)
                         .Select(x => new CategoriasDto()
                         {
                             Id = x.Id,
                             Nombre = x.Nombre,
                             Descripcion = x.Descripcion
                         })
                         .ToList();
                }
                catch (Exception)
                {
                    throw;
                }
            }

            return autores;
        }

        public async Task<bool> CreateRelationAutor(int autorId, int libroId)
        {
            var result = false;

            using (var context = new ApplicationDbContext())
            {
                Autor _autor = context.Autores.FirstOrDefault(x => x.Id == autorId);
                Libro _libro = context.Libros.FirstOrDefault(x => x.Id == libroId);

                if (_autor == null || _libro == null)
                {
                    return result;
                }
                else
                {
                    AutorLibro autorLibroIsExist = await context.AutorLibros
                        .FirstOrDefaultAsync(x => x.AutorId == autorId && x.LibroId == libroId);

                    if (autorLibroIsExist != null)
                    {
                        return result;
                    }

                    AutorLibro autorLibro = new AutorLibro()
                    {
                        AutorId = _autor.Id,
                        LibroId = _libro.Id
                    };
                    context.AutorLibros.Add(autorLibro);

                    try
                    {
                        await context.SaveChangesAsync();
                        result = true;
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }

            return result;
        }

        public async Task<bool> CreateRelationCategoria(int categoriaId, int libroId)
        {
            var result = false;

            using (var context = new ApplicationDbContext())
            {
                Categoria _categoria = context.Categorias.FirstOrDefault(x => x.Id == categoriaId);
                Libro _libro = context.Libros.FirstOrDefault(x => x.Id == libroId);

                if (_categoria == null || _libro == null)
                {
                    return result;
                }
                else
                {
                    CategoriaLibro categoriaLibroIsExist = await context.CategoriaLibros
                        .FirstOrDefaultAsync(x => x.CategoriaId == categoriaId && x.LibroId == libroId);

                    if (categoriaLibroIsExist != null)
                    {
                        return result;
                    }

                    CategoriaLibro categoriaLibro = new CategoriaLibro()
                    {
                        CategoriaId = _categoria.Id,
                        LibroId = _libro.Id
                    };
                    context.CategoriaLibros.Add(categoriaLibro);

                    try
                    {
                        await context.SaveChangesAsync();
                        result = true;
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }

            return result;
        }

        public async Task<bool> DeleteAutor(int autorId, int libroId)
        {
            var result = false;
            
            using (var context = new ApplicationDbContext())
            {
                AutorLibro autorLibro = context.AutorLibros
                    .FirstOrDefault(x => x.AutorId == autorId && x.LibroId == libroId);

                context.AutorLibros.Remove(autorLibro);

                try
                {
                    await context.SaveChangesAsync();
                    result = true;
                }
                catch (Exception)
                {
                    throw;
                }
            }

            return result;
        }

        public async Task<bool> DeleteCategoria(int categoriaId, int libroId)
        {
            var result = false;

            using (var context = new ApplicationDbContext())
            {
                CategoriaLibro categoriaLibro = context.CategoriaLibros
                    .FirstOrDefault(x => x.CategoriaId == categoriaId && x.LibroId == libroId);

                context.CategoriaLibros.Remove(categoriaLibro);

                try
                {
                    await context.SaveChangesAsync();
                    result = true;
                }
                catch (Exception)
                {
                    throw;
                }
            }

            return result;
        }

        public async Task<bool> Delete(int id)
        {
            var result = false;
            Libro libro = Find(id);

            if (libro == null)
            {
                return result;
            }
            else
            {
                using (var context = new ApplicationDbContext())
                {
                    context.Libros.Remove(libro);

                    try
                    {
                        await context.SaveChangesAsync();
                        result = true;
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }

            return result;
        }

        public Libro Find(int id)
        {
            Libro result = null;

            using (var conext = new ApplicationDbContext())
            {
                try
                {
                    result = conext.Libros.FirstOrDefault(x => x.Id == id);
                }
                catch (Exception)
                {
                    throw;
                }
            }

            return result;
        }

        public List<Libro> FindAll()
        {
            List<Libro> result = new List<Libro>();

            using (var conext = new ApplicationDbContext())
            {
                try
                {
                    result = conext.Libros.ToList();
                }
                catch (Exception)
                {
                    throw;
                }
            }

            return result;
        }

        public async Task<bool> Update(Libro libro)
        {
            bool result = false;
            Libro _libro = Find(libro.Id);

            if (_libro == null)
            {
                return result;
            }
            else
            {
                using (var conext = new ApplicationDbContext())
                {
                    conext.Libros.Update(libro);

                    try
                    {
                        await conext.SaveChangesAsync();
                        result = true;
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }

            return result;
        }
    }
}
