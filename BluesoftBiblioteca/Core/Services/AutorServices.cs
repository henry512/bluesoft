﻿using Core.Contracts;
using Infrastructure;
using Infrastructure.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Services
{
    public class AutorServices : IServices<Autor>
    {
        public async Task<Autor> Create(Autor autor)
        {
            Autor result = null;

            using (var context = new ApplicationDbContext())
            {
                context.Autores.Add(autor);

                try
                {
                    await context.SaveChangesAsync();
                    result = autor;
                }
                catch (Exception)
                {
                    throw;
                }
            }

            return result;
        }

        public async Task<bool> Delete(int id)
        {
            var result = false;
            Autor autor = Find(id);

            if (autor == null)
            {
                return result;
            }
            else
            {
                using (var context = new ApplicationDbContext())
                {
                    context.Autores.Remove(autor);

                    try
                    {
                        await context.SaveChangesAsync();
                        result = true;
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }

            return result;
        }

        public Autor Find(int id)
        {
            Autor result = null;

            using (var conext = new ApplicationDbContext())
            {
                try
                {
                    result = conext.Autores.FirstOrDefault(x => x.Id == id);
                }
                catch (Exception)
                {
                    throw;
                }
            }

            return result;
        }

        public List<Autor> FindAll()
        {
            List<Autor> result = new List<Autor>();

            using (var conext = new ApplicationDbContext())
            {
                try
                {
                    result = conext.Autores.ToList();
                }
                catch (Exception)
                {
                    throw;
                }
            }

            return result;
        }

        public async Task<bool> Update(Autor autor)
        {
            bool result = false;
            Autor _autor = Find(autor.Id);

            if (_autor == null)
            {
                return result;
            }
            else
            {
                using (var conext = new ApplicationDbContext())
                {
                    conext.Autores.Update(autor);

                    try
                    {
                        await conext.SaveChangesAsync();
                        result = true;
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }

            return result;
        }
    }
}
