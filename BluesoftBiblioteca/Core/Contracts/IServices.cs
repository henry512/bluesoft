﻿using Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Contracts
{
    interface IServices<Entity>
    {
        List<Entity> FindAll();
        Entity Find(int id);
        Task<Entity> Create(Entity entity);
        Task<bool> Update(Entity entity);
        Task<bool> Delete(int id);
    }
}
