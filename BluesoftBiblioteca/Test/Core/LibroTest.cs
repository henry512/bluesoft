﻿using Core.Services;
using Infrastructure.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Test.Core
{
    [TestClass]
    public class LibroTest
    {

        [TestMethod]
        public async Task Create()
        {
            LibroServices services = new LibroServices();

            Libro libro = new Libro
            {
                Nombre = "50 sombras de gray",
                Isbn = "45-3245-632-23"
            };

            await services.Create(libro);
        }

        [TestMethod]
        public async Task CreateRelationAutor()
        {
            LibroServices services = new LibroServices();

            await services.CreateRelationAutor(2, 2);
        }

        [TestMethod]
        public async Task CreateRelationCategoria()
        {
            LibroServices services = new LibroServices();

            await services.CreateRelationCategoria(1, 1);
        }

        [TestMethod]
        public async Task Update()
        {
            LibroServices services = new LibroServices();

            Libro libro = new Libro
            {
                Id = 1,
                Nombre = "50 sombras de gray Remake",
                Isbn = "45-3245-632-23"
            };

            await services.Update(libro);
        }

        [TestMethod]
        public void FindAll()
        {
            LibroServices services = new LibroServices();

            List<Libro> libros = services.FindAll();

            Console.WriteLine(libros);
        }
    }
}
