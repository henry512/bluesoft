﻿using Core.Services;
using Infrastructure.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Test.Core
{
    [TestClass]
    public class CategoriaTest
    {

        [TestMethod]
        public async Task Create()
        {
            CategoriaServices services = new CategoriaServices();

            Categoria categoria = new Categoria
            {
                Nombre = "drama",
                Descripcion = "categoria de drama"
            };

            await services.Create(categoria);
        }

        [TestMethod]
        public async Task Update()
        {
            CategoriaServices services = new CategoriaServices();

            Categoria categoria = new Categoria
            {
                Id = 1,
                Nombre = "drama",
                Descripcion = "descripción de la categoria de drama"
            };

            await services.Update(categoria);
        }

        [TestMethod]
        public void FindAll()
        {
            CategoriaServices services = new CategoriaServices();

            List<Categoria> categorias = services.FindAll();

            Console.WriteLine(categorias);
        }
    }
}
