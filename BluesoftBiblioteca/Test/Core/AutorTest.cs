﻿using Core.Services;
using Infrastructure.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Test.Core
{
    [TestClass]
    public class AutorTest
    {
        [TestMethod]
        public async Task Create()
        {
            AutorServices services = new AutorServices();

            Autor autor = new Autor
            {
                Nombres = "henry",
                Apellidos = "jaimes",
                Fecha = new DateTime(2020, 04, 13)
            };

            await services.Create(autor);
        }

        [TestMethod]
        public async Task Update()
        {
            AutorServices services = new AutorServices();

            Autor autor = new Autor
            {
                Id = 2,
                Nombres = "henry eduardo",
                Apellidos = "jaimes perez",
                Fecha = new DateTime(2020, 04, 14)
            };

            await services.Update(autor);
        }

        [TestMethod]
        public void FindAll()
        {
            AutorServices services = new AutorServices();

            List<Autor> autor = services.FindAll();

            Console.WriteLine(autor);
        }
    }
}
