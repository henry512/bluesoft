﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Services;
using Infrastructure.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AutorController : ControllerBase
    {
        private readonly AutorServices _autorServices;
        public AutorController(AutorServices autorServices)
        {
            _autorServices = autorServices;
        }

        [HttpGet]
        public ActionResult<List<Autor>> Get()
        {
            return _autorServices.FindAll();
        }

        [HttpGet("{id}")]
        public ActionResult<Autor> Get(int id)
        {
            Autor autor = _autorServices.Find(id);

            if (autor == null)
            {
                return NotFound();
            }

            return Ok(autor);
        }

        [HttpPost]
        public async Task<ActionResult<Autor>> Post([FromBody] Autor autor)
        {
            Autor _autor = null;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _autor = await _autorServices.Create(autor);

            if (_autor == null)
            {
                return BadRequest();
            }

            return CreatedAtAction("Get", _autor);

        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] Autor autor)
        {
            if (id != autor.Id)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _autorServices.Update(autor);

            if (!result)
            {
                return BadRequest();
            }

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var result = await _autorServices.Delete(id);

            if (!result)
            {
                return BadRequest();
            }

            return NoContent();
        }
    }
}
