﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Dto;
using Core.Services;
using Infrastructure.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LibroController : ControllerBase
    {
        private readonly LibroServices _libroServices;

        public LibroController(LibroServices libroServices)
        {
            _libroServices = libroServices;
        }

        [HttpGet]
        public ActionResult<List<Libro>> Get()
        {
            return _libroServices.FindAll();
        }

        [HttpGet("{id}")]
        public ActionResult<Libro> Get(int id)
        {
            Libro libro = _libroServices.Find(id);

            if (libro == null)
            {
                return NotFound();
            }

            return Ok(libro);
        }

        [HttpGet("{id}/autores")]
        public ActionResult<List<AutoresDto>> GetAutores(int id)
        {
            List<AutoresDto> autoresLibro = _libroServices.FindAutores(id);

            if (autoresLibro == null)
            {
                return NotFound();
            }

            return Ok(autoresLibro);
        }

        [HttpGet("{id}/categorias")]
        public ActionResult<List<CategoriasDto>> GetCategorias(int id)
        {
            List<CategoriasDto> categoriasLibros = _libroServices.FindCategorias(id);

            if (categoriasLibros == null)
            {
                return NotFound();
            }

            return Ok(categoriasLibros);
        }

        [HttpPost("{id}/autores")]
        public async Task<ActionResult> PostAutores(int id, [FromBody] AutorLibro autorLibro)
        {
            if (id != autorLibro.LibroId)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result_relation = await _libroServices.CreateRelationAutor(autorLibro.AutorId, autorLibro.LibroId);

            if (!result_relation)
            {
                return BadRequest();
            }

            return NoContent();
        }

        [HttpPost("{id}/categorias")]
        public async Task<ActionResult> PostCategorias(int id, [FromBody] CategoriaLibro categoriaLibro)
        {
            if (id != categoriaLibro.LibroId)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result_relation = await _libroServices.CreateRelationCategoria(categoriaLibro.CategoriaId, categoriaLibro.LibroId);

            if (!result_relation)
            {
                return BadRequest();
            }

            return NoContent();
        }

        [HttpDelete("{id}/autores/{autorId}")]
        public async Task<ActionResult> DeleteAutores(int id, int autorId)
        {
            var result = await _libroServices.DeleteAutor(autorId, id);

            if (!result)
            {
                return BadRequest();
            }

            return NoContent();
        }

        [HttpDelete("{id}/categorias/{categoriaId}")]
        public async Task<ActionResult> DeleteCategorias(int id, int categoriaId)
        {
            var result = await _libroServices.DeleteCategoria(categoriaId, id);

            if (!result)
            {
                return BadRequest();
            }

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<Libro>> Post([FromBody] Libro libro)
        {
            Libro _libro = null;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _libro = await _libroServices.Create(libro);

            if (_libro == null)
            {
                return BadRequest();
            }

            return CreatedAtAction("Get", _libro);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] Libro libro)
        {
            if (id != libro.Id)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _libroServices.Update(libro);

            if (!result)
            {
                return BadRequest();
            }

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var result = await _libroServices.Delete(id);

            if (!result)
            {
                return BadRequest();
            }

            return NoContent();
        }
    }
}
